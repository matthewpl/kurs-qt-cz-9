#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QString>

int main(int argc, char *argv[])
{
	QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");

	db.setHostName("localhost");
	db.setDatabaseName("qt");
	db.setUserName("login");
	db.setPassword("haslo");

	if (!db.open())
	{
		qDebug() << "Błąd: nie można się połączyć z bazą!";
	}
	else
	{
		qDebug() << "Nawiązano połączenie z bazą danych.";

		QSqlQuery dodawanie;
		dodawanie.exec("INSERT INTO test (imie, nazwisko, wiek) VALUES ('Stefan', 'Kowalski', '20')");
		dodawanie.exec("INSERT INTO test (imie, nazwisko, wiek) VALUES ('Jan', 'Nowak', '36')");

		QSqlQuery pobieranie;
		pobieranie.exec("SELECT * FROM test");

		while (pobieranie.next())
		{
			QString imie = pobieranie.value(1).toString();
			QString nazwisko = pobieranie.value(2).toString();
			int wiek = pobieranie.value(3).toInt();
			qDebug() << imie << nazwisko << wiek;
		}
	}

	return 0;
}
